Author: Mason Jones

Contact: masonj@uoregon.edu

Purpose: The goal of this project is to implement the same "file checking" logic that we implemented in project 1 using flask. This project is also a good intro to using Docker.
Like project 1, if a file ("name.html") exists, we transmit "200/OK" header followed by that file html. If the file doesn't exist, we transmit an error code in the header along with the appropriate page html in the body. We are supposed to do this by creating error handlers taught in class. We also create the following two html files with the error messages.

* "404.html" will display "File not found!"

* "403.html" will display "File is forbidden!"
