from flask import Flask, render_template, request

import os.path

app = Flask(__name__)

@app.route("/")
def hello():
	
	name = request.args.get("name")
	pathname = os.path.join("A:/Git/proj2-pageserver/web/templates", name)
	print(pathname)

	if name.startswith('//'):		#if file starts w illegal char
		return render_template("403.html")
	elif name.startswith('~'):		#if file starts w illegal char
		return render_template("403.html")
	elif name.startswith('..'):		#if file starts w illegal char
		return render_template("403.html")
	else:		#if file name is ok
		if os.path.isfile(pathname):		#if path/file exists
			return render_template("nameok.html", name=name)
		else:		#no file found
			return render_template("404.html")

#@app.errorhandler(404)
#def error_404(404):
#	return render_template("404.html")

#@app.errorhandler(403)
#def error_403(403):
#	return render_template("403.html")

#Original Code
#def hello():
#   return "UOCIS docker demo!"
#Original Code
if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0')
